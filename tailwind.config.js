/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    colors: {      
      transparent: 'transparent',
      current: 'currentColor',
      'white': '#ffffff',
      'orange': '#F17400',
      'indigo': '#5A38FD',
      'green': '#2CD997',
      'red': '#FF0000'
    },
    extend: {},
  },
  plugins: [],
}

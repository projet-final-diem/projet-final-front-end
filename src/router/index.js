import { createRouter, createWebHistory } from 'vue-router'
import LoginComponent from '@/views/view-login.vue'
import HomeComponent from '@/views/view-home.vue'
import ChoiceComponent from '@/views/view-choice.vue'
import CalendarComponent from '@/views/view-calendars.vue'
import OneCalendarComponent from '@/views/view-one-calendar.vue'
import TrophyComponent from '@/views/view-trophy.vue'
import CreateAccountComponent from '@/views/view-create-account.vue'
import WelcomeComponent from '@/views/view-welcome.vue'
import ThinkComponent from '@/views/view-thinking.vue'



const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeComponent
    },
    {
      path: '/think',
      name: 'think',
      component: ThinkComponent
    },
    {
      path: '/choice',
      name: 'choice',
      component: ChoiceComponent
    },
    {
      path: '/calendars',
      name: 'calendars',
      component: CalendarComponent
    },
    {
      path: "/calendar/:id",
      name: 'singleCalendar',
      component: OneCalendarComponent
    },
    {
      path: '/trophy',
      name: 'trophy',
      component: TrophyComponent
    },
    {
      path: '/login',
      name: 'login',
      component: LoginComponent
    },
    {
      path: '/create-account',
      name: 'createAccount',
      component: CreateAccountComponent
    },
    {
      path: '/welcome',
      name: 'welcome',
      component: WelcomeComponent
    }

  ]
})

export default router

import { defineStore } from 'pinia'

export const useUserAuth = defineStore({
  id: 'UserAuth',
  state: () => ({
    user: JSON.parse(localStorage.getItem('user'))
  }),
  actions: {
    setUser(logged) {
      this.user = logged;
    },
    getUser() {
      return this.user;
    },
    deleteUser() {
      this.user = null;
    }
  }
})

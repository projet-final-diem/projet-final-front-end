import axios from "axios"
const API_URL = 'http://localhost:8080/habit'
import {useUserAuth} from '../stores/user'

class HabitService {
  async fetchHabits() {
    const id = useUserAuth().user.id;
    const response = await axios
      .get(API_URL + "/user/" + id)
      return response.data;
  }
}

export default new HabitService();
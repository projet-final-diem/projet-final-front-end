import axios from 'axios';
import { useUserAuth } from '../stores/user';

const API_URL = 'http://localhost:8080/user';

class AuthService {

  async login(user) {
    const response = await axios
      .get(API_URL + '/id/login', { withCredentials: true,auth: {
        username: user.email,
        password: user.password,
      } });

      localStorage.setItem('user', JSON.stringify(response.data));
      useUserAuth().setUser(response.data);
  }

  logout() {
    localStorage.removeItem('user');
    useUserAuth().deleteUser();
  }

  async register(user) {
    await axios
      .post(API_URL, {
        userName: user.username,
        email: user.email,
        password: user.password
      });
  }
}

export default new AuthService();

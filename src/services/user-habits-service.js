import axios from 'axios'
import {useUserAuth} from '../stores/user'
import { storeToRefs } from 'pinia'

const API_URL = 'http://localhost:8080/userhabit'


class UserHabitService {

  async fetchUserHabitByDay() { 
    if (useUserAuth().user != null) {
      const id = useUserAuth().user.id
      console.log("panda content");
      const response = await axios
      .get(API_URL + '/today/' + id);
      return response.data;
    }
  }

  async createUserHabit(habit) {
    const response = await axios
      .post(API_URL, {
        userId: useUserAuth().user.id,
        habitId: habit.id,
        label: habit.label,
        icon: habit.icon
      });
      return response.data;
  }
}

export default new UserHabitService();

import axios from "axios"

const API_URL = 'http://localhost:8080/habit-status'

class HabitStatusService {
  
  async updateUserHabit(habitStatus) {
    const response = await axios
      .put(API_URL, {
        id: habitStatus.id,
        forDate: habitStatus.forDate,
        status: habitStatus.status,
        userHabitID: habitStatus.userHabitID
      });
  }

  async createUserHabit(habitStatus) {
    const response = await axios
      .post(API_URL, {
        forDate: habitStatus.forDate,
        status: habitStatus.status,
        userHabitID: habitStatus.userHabitID
      });
  }
}

export default new HabitStatusService();